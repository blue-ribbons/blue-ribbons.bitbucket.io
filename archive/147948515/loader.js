(function (w,d,n) {
    /* Another loader is already loaded */
    if (!w[n].enabled) { w[n].enabled = true; } else { return; }

    var req  = ['c_id', 'si_id', 'se_id', 'se_type', 'width', 'height'];
    var used = [];

    w[n] = w[n] || [];

    /* Iterate over registered sections */
    for (var i = 0; i < w[n].length; i++) {
        if (i.constructor !== Number) { return; }

        var el = w[n][i];
        var par = [];
        var docEl, k, j, script, xhr, docIf, iframe, key;

        /* Check for required section params in here */
        for (j in req) {
            if (!(req[j] in el)) { return; }
            par.push([req[j], '=', el[req[j]]].join(''));
        }

        /* Check if section has not been registered twice - create unique identifier */
        if (!~used.indexOf(key = [el.se_id, '_', el.si_id, '_', el.se_type].join(''))) { used.push(key); } else { return; }

        (div = d.getElementById(el.c_id)) && (iframe = d.createElement('iframe'));

        /* ENV-independent URL setting */
        var src   = 'http://' + el.pub_url + '/' + el.se_type;
        var style = {border:  '0px solid black', margin:  '0', padding: '0'};
        var props = {id: 'iframe_' + el.c_id, height: el.height, width:  el.width, scrolling: 'no', frameborder: 0};

        for (k in style) { iframe.style[k] = style[k]; }
        for (k in props) { iframe[k] = props[k]; }

        /* Iframe setup */
        div.appendChild(iframe);

        iframe.src = 'javascript:void((function(){var script = document.createElement(\'script\');' +
                     'script.innerHTML = "(function() {document.open();document.domain=\''          +
                         this.document.domain                                                       +
                     '\';document.close();})();";'                                                  +
                     'document.write(script.outerHTML);})())';

        docIf = (iframe.contentWindow || iframe.contentDocument);
        docIf = (docIf.document || docIf);
        docIf.write('<head></head>');
        docIf.write('<body style="margin:0px;padding:0px;">');
        docIf.write('<scr' + 'ipt src="' + src + '?' + par.join('&') + '" ></scr' + 'ipt>');
        docIf.write('<script type="text/javascript">document.close();</script>');
        docIf.write('</body>');
    }

    w[n].used = used;

    return null;
})(window, document, 'fogzy_container_v1');
