local src = require("lang/"..about.settings.lang.."/brm-2")
script = {
	[1] = { "set_db_style", 2 },
	[2] = { "set_page", { name = "BRM2 - Dark Alley", image = "cg-4/33" } },
	[3] = { "add_layer", { name = "chiyo", img = "blue-chiyo2", y = 600 } },
	[4] = { "play", { mode = 1, name = "gsnk09", loop = true } },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "print", { msg = src[10], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[11], name = "brm-2-3" } } },
}
return script