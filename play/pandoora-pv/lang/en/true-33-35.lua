local src = {}
src[1] = '"Should I really end it like that?" - Nozaki mumbles to himself in his slumber.\n"Is it too happy of an ending? Hmm, it may seem a bit unrealistic after all."'
src[2] = '"But if I end everything that happened as a dream, they\'ll call me a talentless hack. Maybe I should call Chiyo-chan and see what she thinks."'
src[3] = '"And if I don\'t end it in the near future, they\'re going to say I\'m milking my work for all it\'s worth."'
return src
