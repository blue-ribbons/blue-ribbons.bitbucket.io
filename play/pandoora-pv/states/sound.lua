local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local sound = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function sound:enter(state)	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)	
	about.scene = "sound"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.sfx = game.sfx
	self.activeColor = game.activeColor
	
	self.menu = { sections = lang.sound, labels = lang.switch, radio = { [1] = true, [2] = false }, buttons = {}, actions = {}, active = 1 }
	self.radioGroup = {}
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do	
		gooi.newRadio("sound-"..i, self.menu.labels[i], (sW / 5) * 2 - 170 * sc, (210 + delta * i) * sc, 245 * sc, 60 * sc, self.menu.radio[i], "bgm", "sound-radio")
		table.insert(self.menu.buttons, "sound-"..i)
	end
	
	for i = 1, #self.menu.labels, 1 do	
		gooi.newRadio("sound-"..(i + 2) , self.menu.labels[i], (sW / 5) * 3 - 90 * sc, (210 + delta * i) * sc, 245 * sc, 60 * sc, self.menu.radio[i], "sfx", "sound-radio")
		table.insert(self.menu.buttons, "sound-"..(i + 2))
	end
	
	gooi.newButton("sound-5", lang.start, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("sound-5").howRound = 1
	gooi.get("sound-5"):generateBorder()
	table.insert(self.menu.buttons, "sound-5")
	
	self.menu.actions[1] = function()
		local config = { settings = about.settings }
		love.filesystem.write("pager-settings.cfg", json.encode(config))
		GS.switch(require("states/mainmenu"))
	end
	
	self.onKeyDown = function()	
		gooi.get("sound-"..self.menu.active).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get("sound-"..self.menu.active).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		
		if self.menu.active < #self.menu.buttons then
			gooi.get("sound-"..self.menu.active):select()
		else
			self.menu.actions[1]()
		end
		
	end
	
	gooi.get("sound-5"):onRelease(function(c) TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]() end)
	
	self.timer = timer.new()
	self.joystickBlocked = false	
end

function sound:leave()
	self.timer:clear()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("sound-"..i)
	end
end

function sound:update(dt)
	gooi.get("sound-"..self.menu.active).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end		
	end
	
	about.settings.sound.bgm = gooi.get("sound-1").selected
	about.settings.sound.sfx = gooi.get("sound-3").selected
	self.timer:update(dt)	
end

function sound:draw()
	love.graphics.printf(lang.sound[1].."/"..lang.sound[2], 0, 190 * sc, sW, 'center')
	gooi.draw()
	if about.settings.gpswitch then
		gooi.draw("gamepad")
	end
	gooi.draw("sound-radio")	
	drawFPS()
end

function sound:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.onKeyUse()	end,
		[" "] = function()	self.onKeyUse()	end,
		["e"] = function()	self.onKeyUse()	end,
		["x"] = function()	self.onKeyUse()	end,
	}
	if switch[key] then switch[key]()	end
end

function sound:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.onKeyUse()	end,
		[2] = function()	self.onKeyUse()	end,
		[3] = function()	self.onKeyUse()	end,
		[4] = function()	self.onKeyUse()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end,
	}
	if switch[button] then switch[button]()	end
end
return sound