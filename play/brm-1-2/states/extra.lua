local timer = require("libs/hump/timer")
local extra = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function extra:init()
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.sfx = game.sfx
	self.timer = timer.new()	
end

function extra:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "extra"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.menu = { labels = lang.extra, buttons = {}, active = 1 }
	self.menu.actions = {
		[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/gallery"))	end,
		[2] = function()	TEsound.play(self.sfx[1], "click", 0.6)	TEsound.stop("bgm")	GS.switch(require("states/mplayer"))	end,
		[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/edlist"))	end,
		[4] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/mainmenu"))	end,
	}
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do		
		gooi.newButton("extra-"..i, self.menu.labels[i], sW / 2 - 140 * sc, (270 + delta * i) * sc, 280 * sc, 60 * sc)	--// (340 + delta * i)
		table.insert(self.menu.buttons, "extra-"..i)	
	end
	for i = 1, #self.menu.buttons, 1 do	
		gooi.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	gooi.newButton("extra-4", lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("extra-4").howRound = 1
	gooi.get("extra-4"):generateBorder()
	gooi.get("extra-4"):onRelease(self.menu.actions[4])
	table.insert(self.menu.buttons, "extra-4")
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
end

function extra:leave()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("extra-"..i)
	end
end

function extra:update(dt)
	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function extra:draw()
	love.graphics.setFont(self.font)
	love.graphics.printf(game.title, 0, 120, sW, 'center')
	love.graphics.setFont(game.fonts["small-web"])
	love.graphics.printf("1st Anniversary Edition", 0, 220, sW, 'center')
	gooi.draw()	
	drawFPS()	
end

function extra:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[4]()	end,
		["menu"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[4]()	end,
	}
	if switch[key] then switch[key]()	end
end

function extra:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[4]()		end,
	}
	if switch[button] then switch[button]()	end	
end
return extra