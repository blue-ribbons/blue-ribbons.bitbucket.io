local timer = require("libs/hump/timer")
local gameoptions = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function gameoptions:init()	
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx
end

function gameoptions:enter(state)
	about.scene = "gameoptions"	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end	
	self.bgm = TEsound.playLooping("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
	TEsound.volume(self.bgm, 0.5)
	
	self.quit = function()
		TEsound.stop("bgm")
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/gamescreen"), true)
	end
	
	gooi.newButton("menu_btn", _, xc - (64 * sc + 12 * sc), 12 * sc, 64 * sc, 64 * sc, "graphics/static/gear.png", "menu", sc)
	gooi.get("menu_btn"):onRelease(function() self.quit() end)
	
	self.menu = { labels = {}, buttons = {}, active = 1 }
	self.menu.labels = lang.gamemenu
	self.menu.actions = {
		[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/save"))	end,
		[2] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/load"))	end,
		[3] = function()	TEsound.stop("bgm")
							TEsound.play(self.sfx[1], "click", 0.6)
							local bgm = TEsound.playLooping("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
							TEsound.volume(bgm, 0.5)
							about.session.pgr_image = nil
							GS.switch(require("states/mainmenu"))
							end,
	}
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do
		
		gooi.newButton("menu-"..i, self.menu.labels[i], sW / 2 - 120 * sc, (340 + delta * i) * sc, 240 * sc, 60 * sc, _, "menu")
		table.insert(self.menu.buttons, "menu-"..i)
		
	end
	
	for i = 1, #self.menu.buttons, 1 do
		gooi.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
	
end

function gameoptions:update(dt)
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
		
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end			
	end
	self.timer:update(dt)
end

function gameoptions:draw()
	love.graphics.setFont(self.font)
	love.graphics.printf(game.title, 0, 120, sW, 'center')
	gooi.draw("menu")
	drawFPS()
end

function gameoptions:leave()	
	self.timer:clear()	
	gooi.remove("menu_btn")
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("menu-"..i)
	end
end

function gameoptions:keypressed(key, unicode)

	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
	
end

function gameoptions:joystickpressed(joystick, button)
	
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
		
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end
	
end
return gameoptions