local src = require("lang/"..about.settings.lang.."/true-10")
script = {
	[1] = { "clear" },
	[2] = { "set_page", { name = "Dark Alley 9", image = "cg-1/9" } },
	[3] = { "play", { mode = 1, name = "se003", loop = true } },
	[4] = { "fade_text", { dir = "in", time = 1 } },
	[5] = { "set_page", { name = "Dark Alley 10", image = "cg-1/10" }, { mode = "fade", time = 3 } } ,
	[6] = { "fade_text", { dir = "out", time = 0.5 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "input", { type = 0 }},
	[9] = { "fade_text", { dir = "in", time = 1 } },
	[10] = { "zoom_page", { dir = "in", coords = "center-center", amount = 1.5, time = 1 } },
	[11] = { "rotate", { speed = 1, cont = true } },
	[12] = { "fill", { mode = "in", r = 255, g = 255, b = 255, time = 1 } },
	[13] = { "read", { name = "true-11" } },
}
return script