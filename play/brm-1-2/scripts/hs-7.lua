local src = require("lang/"..about.settings.lang.."/hs")
script = {
	[1] = { "set_db_style", 2 },
	[2] = { "set_page", { name = "Halloween - Dark Alley", image = "cg-4/36" } },
	[3] = { "play", { mode = 0, id = 26 } },
	[4] = { "set_page", { name = "Halloween - Dark Alley", image = "cg-4/37" }, { mode = "fade", time = 0.5 } },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "fade_text", { dir = "in", time = 0.5 } },
	[7] = { "clear" },
	[8] = { "set_page", { name = "Halloween - Costume Show", image = "cg-4/38" }, { mode = "flip", dir = "right", time = 1 } },
	[9] = { "fade_text", { dir = "out", time = 1 } },
	[10] = { "print", { msg = src[21], mode = 0 } },
	[11] = { "wait", { time = 0.5 } },
	[12] = { "input", { type = 1 }, { [1] = { text = src[22], name = "hs-8" } } },
}
return script