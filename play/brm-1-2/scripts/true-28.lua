local src = require("lang/"..about.settings.lang.."/true-28")
script = {
	[1] = { "set_page", { name = "At School 2", image = "cg-1/27" }} ,
	[2] = { "set_page", { name = "At School 3", image = "cg-1/28" }, { mode = "fade", time = 1 }},
	[3] = { "play", { mode = 0, id = 7 } },
	[4] = { "unlock", { type = 1, id = 2 } },
	[5] = { "fade_text", { dir = "out", time = 0.5 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "print", { msg = src[2], mode = 1 } },
	[9] = { "wait", { time = 0.5 } },
	[10] = { "input", { type = 0 } } ,
	[11] = { "fade_text", { dir = "in", time = 0.5 } },
	[12] = { "clear" },
	[13] = { "read", { name = "true-29" } },
}
return script