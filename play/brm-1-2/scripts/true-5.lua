local src = require("lang/"..about.settings.lang.."/true-5")
script = {
	[1] = { "set_page", { name = "Dark Alley 4", image = "cg-1/4" } },
	[2] = { "print", { msg = src[1], mode = 0 } },
	[3] = { "wait", { time = 0.5 } },
	[4] = { "fade_text", { dir = "in", time = 1 } },
	[5] = { "set_page", { name = "Dark Alley 5", image = "cg-1/5" }, { mode = "fade", time = 1 } } ,
	[6] = { "fade_text", { dir = "out", time = 1 } },
	[7] = { "print", { msg = src[2], mode = 1 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "input", { type = 1 }, { [1] = { text = src[4], name = "true-6" }, [2] = { text = src[5], name = "true-6" }}},
}
return script