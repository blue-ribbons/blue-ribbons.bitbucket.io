local src = require("lang/"..about.settings.lang.."/rebuild-12-13")
script = {
	[1] = { "play", { mode = 1, name = "angeloid_no_senaka", loop = true } },
	[2] = { "set_page", { name = "Disposal", image = "cg-2/12" } },
	[3] = { "fill", { mode = "out", time = 5 } },
	[4] = { "unlock", { type = 2, id = 7 } },
	[5] = { "fade_text", { dir = "out", time = 2 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "input", { type = 0 } },
	[8] = { "print", { msg = src[2], mode = 1 } },
	[9] = { "input", { type = 0 } },
	[10] = { "fade_text", { dir = "in", time = 1 } },
	[11] = { "clear" },
	[12] = { "play", { mode = 0, id = 10 } },
	[13] = { "set_page", { name = "Disposal", image = "cg-2/13" }, { mode = "fade", time = 1 } },
	[14] = { "wait", { time = 1.5 } },
	[15] = { "fill", { mode = "in", time = 3, r = 255, g = 0, b = 0 } },
	[16] = { "read", { name = "rebuild-14-17" } },
}
return script