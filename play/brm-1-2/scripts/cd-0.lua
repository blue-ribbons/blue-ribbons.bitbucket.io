local src = require("lang/"..about.settings.lang.."/cd")
script = {
	[1] = { "set_page", { name = "Chaika Dojo", image = "cg-2/cd-0" }},
	[2] = { "fill", { mode = "in", instant = true } },
	[3] = { "fill", { mode = "out", time = 3 } },
	[4] = { "clear" },
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[2], name = "cd" }, [2] = { text = src[2], name = "cd" } } },
}
return script