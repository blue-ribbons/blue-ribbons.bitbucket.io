local src = require("lang/"..about.settings.lang.."/true-24")
script = {
	[1] = { "set_page", { name = "Dark Alley 23", image = "cg-1/23" }} ,
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "set_page", { name = "Dark Alley 24", image = "cg-1/24" }, { mode = "fade", time = 0.5 }} ,
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "print", { msg = src[1], mode = 0 } },
	[6] = { "wait", { time = 0.5 } },
	[7] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-25" }, [2] = { text = src[3], name = "true-25" } } },
}
return script