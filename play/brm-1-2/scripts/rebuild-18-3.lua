local src = require("lang/"..about.settings.lang.."/rebuild-18-2")
script = {
	[1] = { "set_page", { name = "Heavy Thoughts 3", image = "cg-2/17" } },
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "clear" },
	[4] = { "set_page", { name = "Heavy Thoughts 4", image = "cg-2/18-3" }, { mode = "flip", dir = "left", time = 1 } },
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[2], name = "mos-2" }, [2] = { text = src[3], name = "rebuild-19-3"} } },
}
return script