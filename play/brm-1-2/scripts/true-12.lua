local src = require("lang/"..about.settings.lang.."/true-12")
script = {
	[1] = { "set_page", { name = "Dark Alley 11", image = "cg-1/11" } },
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "set_page", { name = "Dark Alley 12", image = "cg-1/12" }, { mode = "fade", time = 2 } } ,
	[4] = { "wait", { time = 0.5 } },
	[5] = { "zoom_page", { dir = "in", time = 0.5, amount = 3, coords = "top-left"}},
	[6] = { "play", { mode = 1, name = "katsubou", loop = true } },
	[7] = { "unlock", { type = 2, id = 3 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "zoom_page", { dir = "out", time = 0.5 }},
	[10] = { "fade_text", { dir = "out", time = 0.5 } },
	[11] = { "print", { msg = src[1], mode = 1 } },
	[12] = { "wait", { time = 0.5 } },
	[13] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-13" }, [2] = { text = src[3], name = "true-13" }}},
}
return script