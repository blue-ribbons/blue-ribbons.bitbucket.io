local src = require("lang/"..about.settings.lang.."/true-22")
script = {
	[1] = { "set_page", { name = "Dark Alley 22", image = "cg-1/22" }} ,
	[2] = { "play", { mode = 0, id = 6 } },
	[3] = { "play_cancel" },
	[4] = { "fill", { mode = "out", instant = true } },
	[5] = { "wait", { time = 0.2 } },
	[6] = { "fill", { mode = "in", instant = true } },
	[7] = { "wait", { time = 0.2 } },
	[8] = { "fill", { mode = "out", instant = true } },
	[9] = { "set_page", { name = "Dark Alley 22", image = "cg-1/22" }, { mode = "flip", dir = "up", time = 0.5 }},
	[10] = { "fill", { mode = "in", instant = true } },
	[11] = { "wait", { time = 0.2 } },
	[12] = { "fill", { mode = "out", instant = true } },	
	[13] = { "set_page", { name = "Dark Alley 22", image = "cg-1/22" }, { mode = "flip", dir = "down", time = 1 }},
	[14] = { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[15] = { "fade_text", { dir = "out", time = 2 } },
	[16] = { "print", { msg = src[1], mode = 0 } },
	[17] = { "wait", { time = 0.5 } },
	[18] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-23" }, [2] = { text = src[3], name = "true-23" } } },
}
return script