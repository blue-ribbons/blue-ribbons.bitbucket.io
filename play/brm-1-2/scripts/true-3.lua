local src = require("lang/"..about.settings.lang.."/true-3")
script = {
	[1] = { "set_page", { name = "Dark Alley 2", image = "cg-1/2" } },
	[2] = { "play_cancel" },
	[3] = { "print", { msg = src[1], mode = 0 } },
	[4]	= { "play", { mode = 1, name = "fatal_scene", loop = true } },
	[5] = { "set_page", { name = "Dark Alley 3", image = "cg-1/3" } } ,
	[6] = { "unlock", { type = 2, id = 2 } },	
	[7] = { "print", { msg = src[2], mode = 1 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "input", { type = 1 }, { [1] = { text = src[3], name = "true-3-1" }, [2] = { text = src[4], name = "true-3-1" }, [3] = { text = src[5], name = "true-4" }}},
}
return script