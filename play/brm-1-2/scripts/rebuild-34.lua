local src = require("lang/"..about.settings.lang.."/rebuild-34")
script = {
	[1] = { "set_page", { name = "Nisekoi", image = "cg-2/34" }},
	[2] = { "play", { mode = 1, name = "click_cover_cut", loop = true } },
	[3] = { "unlock", { type = 2, id = 10 } },
	[4] = { "fill", { mode = "out", time = 2,  r = 255, g = 255, b = 255 } },
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[2], name = "rebuild-35" }, [2] = { text = src[3], name = "rebuild-35" } } },	
}	
return script