local src = require("lang/"..about.settings.lang.."/true-29")
script = {
	[1] = { "set_page", { name = "At School 3", image = "cg-1/28" }} ,
	[2] = { "play_cancel" },
	[3] = { "set_page", { name = "At School 4", image = "cg-1/29" }, { mode = "fade", time = 1 }},
	[4] = { "fade_text", { dir = "out", time = 0.5 } },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "play", { mode = 1, name = "nokosare_ta_jikan", loop = true } },
	[7] = { "unlock", { type = 2, id = 5 } },
	[8] = { "print", { msg = src[1], mode = 0 } },
	[9] = { "wait", { time = 1 } },
	[10] = { "print", { msg = src[2], mode = 1 } },
	[11] = { "wait", { time = 0.5 } },
	[12] = { "input", { type = 0 } } ,
	[13] = { "fade_text", { dir = "in", time = 0.5 } },
	[14] = { "clear" },
	[15] = { "read", { name = "true-30-32" } },
}
return script