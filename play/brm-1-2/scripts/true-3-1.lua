local src = require("lang/"..about.settings.lang.."/true-3-1")
script = {
	[1] = { "set_page", { name = "Dark Alley 3", image = "cg-1/3" } },
	[2] = { "print", { msg = src[1], mode = 0 } },
	[3] = { "wait", { time = 0.5 } },
	[4] = { "input", { type = 1 }, { [1] = { text = src[2], name = "true-4" }}},
}
return script