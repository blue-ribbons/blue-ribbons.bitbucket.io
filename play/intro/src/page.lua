--// A "page" instance contains a visual representation of scene (background CG, several overlay layers and animation, as well as in/out transitions and zoom-in/zoom-out effects)
--// All methods below are called by page controller class (pager)

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"
local vector = require "libs/hump/vector"
--local anim8 = require "libs/anim8"

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy = br.cx, br.cy
local za = br.za
local to_string = tostring
--local mfloor = math.floor

page = class {}

function page:init(pg)
	
	local anim_metadata, overlay_metadata = pg.anim_metadata or {}, pg.overlay_metadata or {}
	
	self.name = pg.name
	local imagepath = "graphics/"..pg.image..".jpg"
	self.image = love.graphics.newImage(imagepath)
	self.anim = {}
	self.overlay = {}
--	self.x, self.y, self.r, self.sx, self.sy, self.ox, self.oy = pg.x or  cx + (950 * sc) / 2, pg.y or cy + (720 * sc) / 2, pg.r or 0, pg.sx or 1, pg.sy or 1, pg.ox or (950 * sc) / 2, pg.oy or (720 * sc) / 2
	self.x, self.y, self.r, self.sx, self.sy, self.ox, self.oy = pg.x or  sW / 2, pg.y or sH / 2, pg.r or 0, pg.sx or 1, pg.sy or 1, pg.ox or self.image:getWidth() / 2, pg.oy or self.image:getHeight() / 2
--	self.x, self.y = cx + self.x, cy + self.y
	self.color = pg.color or {	r = 255, g = 255, b = 255, alpha = 255	}
	self.timer = timer.new()
	self.isVisible = true
	self.state = 0	--// 0 - free, 1 - busy
	
--[[	for i = 1, #anim_metadata, 1 do			
		local grid = anim8.newGrid(anim_metadata[i]["tilesize"], anim_metadata[i]["tilesize"], anim_metadata[i]["width"], anim_metadata[i]["height"])
		self.anim[i] = {
			src = love.graphics.newImage(anim_metadata[i]["src"]),
			idle = anim8.newAnimation(grid(anim_metadata[i]["fstart"]..'-'..anim_metadata[i]["ffinish"]..'5', 1), 0.2),
			pos = vector(anim_metadata[i]["x"], anim_metadata[i]["y"])
		}			
	end	]]
	
	for i = 1, #overlay_metadata, 1 do
		self.overlay[i] = love.graphics.newImage(overlay_metadata[i])
	end
	
end

function page:update(dt)
--[[	for i = 1, #self.anim, 1 do
		self.anim[i]["src"]:update(dt)
	end	]]
	self.timer:update(dt)
end

function page:draw()
	
	if self.isVisible then
		love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
		if not(self.sx or self.sy) then self.sx, self.sy = 1, 1	end
		love.graphics.draw(self.image, self.x, self.y, self.r, self.sx * sc, self.sy * sc, self.ox, self.oy)
		for i = 1, #self.overlay, 1 do
			love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
			love.graphics.draw(self.overlay[i], self.x, self.y, self.r, self.sx * sc, self.sy * sc, self.ox, self.oy)
		end
		
--[[		for i = 1, #self.anim, 1 do
			love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
			self.anim[i]["idle"]:draw(self.anim[i]["src"], self.anim[i].pos.x, self.anim[i].pos.y)
		end	]]
		
	end
	
end

function page:hide(time, onComplete)
	local t = time or 2
	local oc = onComplete or function()	self.state = 0	end
	self.timer:tween(t,  self.color, {	alpha = 0	}, "linear", oc)
end

function page:show(time, onComplete)
	local t = time or 2
	local oc = onComplete or function()	self.state = 0	end
	self.timer:tween(t,  self.color, {	alpha = 255	}, "linear", oc)
end

function page:zoomIn(time, coords, amount, onComplete, instant)		--// coords now marks the predefined x and y of area to zoom; amounts less than 2 produce unacceptable results
	local c 
	if coords then
		c = { x = self.x + za[coords]["x"], y = self.y + za[coords]["y"] }
	else
		c = { x = self.x + za["center-center"]["x"], y = self.y + za["center-center"]["y"] }
	end
	print("zoom: "..c.x, c.y)
	local a = amount or 2
	local oc = onComplete or function()	self.state = 0	end
	if not instant then
		local t = time or 2

--		self.timer:tween(t,  self, {	x = -c.x, y = -c.y, sx = a, sy = a	}, "linear", oc)	--// coords should be inverted since we are pointing to a certain point on the screen
		self.timer:tween(t,  self, { sx = a, sy = a }, "linear")
--		self.timer:tween(t,  self, { x = -c.x * a / 2, y = -c.y * a / 2 }, "linear", oc)
		self.timer:tween(t,  self, { x = c.x, y = c.y }, "linear", oc)
	else
		self.x, self.y, self.sx, self.sy = c.x * a / 2, c.y * a / 2, a, a
		oc()
	end
end

function page:zoomOut(time, coords, amount, onComplete, instant)
	local c
	if coords then 
		c = coords
		c.x, c.y = c.x * sc, c.y * sc
	else
		c = { x = cx - (950 * sc) / 2, y = cy - (720 * sc) / 2 }
	end

	local oc = onComplete or function()	self.state = 0	end
	if not instant then
		local t = time or 2
		local a = amount or 1
	--	a = a * sc
		a = 1
	--	self.timer:tween(t,  self, {	x = -c.x, y = -c.y, sx = a, sy = a	}, "linear", oc)	
	--	self.timer:tween(t, self, { x = c.x, y = c.y, sx = a, sy = a }, "linear", oc)
		self.timer:tween(t, self, { sx = a, sy = a }, "linear")
		self.timer:tween(t,  self, { x = cx + (950 * sc) / 2, y = cy + (720 * sc) / 2 }, "linear", oc)
	else
	--	self.x, self.y = c.x, c.y
		self.x, self.y, self.sx, self.sy = c.x, c.y, a, a
		oc()
	end
end

function page:move(time, dir, offset, onComplete)
	local t = time or 2
	local d = dir or "left"
	local o = offset or false
	local oc = onComplete or function()	self.state = 0 end
	local pos = { 0, 0 }
	local switch = {
		["left"] = function()
			if o then self.x = self.x + 950 * sc end
			pos[1], pos[2] = self.x - 950 * sc, self.y					
		end,
		["right"] = function()
			if o then self.x = self.x - 950 * sc	end
			pos[1], pos[2] = self.x + 950 * sc, self.y					
		end,
		["up"] = function()
			if o then self.y = self.y - 720 * sc	end
			pos[1], pos[2] = self.x, self.y	+ 720 * sc				
		end,
		["down"] = function()
			if o then self.y = self.y + 720 * sc	end
			pos[1], pos[2] = self.x, self.y	- 720 * sc				
		end,
	}
	switch[d]()
	self.timer:tween(t,  self, { x = pos[1], y = pos[2] }, "linear", oc)
end

function page:rotate(params)
	self.ox, self.oy = (950 * sc) / 2, (720 * sc) / 2
	self.x, self.y = cx + (950 * sc) / 2, cy + (720 * sc) / 2
	local spd, cont
	if params then
		spd = params.speed or 2
		cont = params.cont or false
	else
		spd, cont = 2, false
	end
	local switch = {
		["true"]	= function()
			local function rotate()
				self.timer:tween(spd, self, { r = 6.283 }, "linear", function() rotate() end)
			end
			self.timer:tween(spd, self, { r = 6.283 }, "linear", function() rotate() end)
		end,
		["false"]	= function()	self.timer:tween(spd, self, { r = 6.283 }, "linear")	end ,
	}
	switch[to_string(cont)]()
end