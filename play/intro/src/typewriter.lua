--// Typewriter prints text and controls overlay/text fading
--// Accepts instructions from pager.

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"
local vector = require "libs/hump/vector"

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy = br.cx, br.cy
local ssub, slen
local large = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 64)

--[[
if about.settings.lang ~= "en" then
	slen = string.utf8len
	ssub = string.utf8sub
else
	slen = string.len
	ssub = string.sub
end	]]

slen = string.utf8len
ssub = string.utf8sub

typewriter = class {}

function typewriter:init()
	
	self.state = 0	--// 0 - free, 1 - busy
	self.color = {	r = 0, g = 0, b = 0, alpha = 127	}
	self.textcolor = {	r = 255, g = 255, b = 255, alpha = 255	}
	self.message = ""		--"Hey! Hey! Nozaki-kun!\nI finished the draft for the dialogue you asked me for :) "
	self.buffer = " "	
	self.timer = timer.new()
	self.align = "left"
	
end

function typewriter:update(dt)
	self.timer:update(dt)
end

function typewriter:draw()
	
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
	love.graphics.rectangle('fill', cx + 176 * sc, cy + 132 * sc, 800 * sc, 600 * sc)
	
	love.graphics.setFont(large)
	love.graphics.setColor(self.textcolor.r, self.textcolor.g, self.textcolor.b, self.textcolor.alpha)
	love.graphics.printf(self.buffer, cx + 206 * sc, cy + 162 * sc, 770 * sc, self.align)
	
end

function typewriter:hide(time, onComplete, instant)
	local oc = onComplete or function()	self.state = 0	end
	if not instant then
		local t = time or 2	
		self.timer:tween(t,  self.color, {	alpha = 0	}, "linear", oc)
		self.timer:tween(t,  self.textcolor, {	alpha = 0	}, "linear", oc)
	else
		self.color.alpha = 0
		self.textcolor.alpha = 0
		oc()
	end
end

function typewriter:show(time, onComplete, instant)
	
	local oc = onComplete or function()	self.state = 0	end
	if not instant then
		local t = time or 2		
		self.timer:tween(t,  self.color, {	alpha = 127	}, "linear", oc)
		self.timer:tween(t,  self.textcolor, {	alpha = 255	}, "linear", oc)
	else
		self.color.alpha = 127
		self.textcolor.alpha = 255
		oc()
	end
end

function typewriter:write(msg, mode, instant)
	local m = mode or 0		--// 0 - rewind buffer, 1 - add new lines
	local switch = {
		[0]	=	function()	self.message = msg	self.buffer = ""	end,
		[1]	=	function()	self.message = self.message.."\n"..msg	end,
	}
	switch[m]()	
	if not instant then
		self.timer:addPeriodic(about.settings.textspd, function()
			self.buffer = ssub(self.message, 1, slen(self.buffer) + 1)
			if slen(self.buffer) >= slen(self.message) then
				 self.state = 0
			end
		end, slen(self.message))
	else
		self.buffer = self.message
		self.state = 0
	end
end

function typewriter:clear()
	self.message = ""
	self.buffer = ""
end

function typewriter:setColor(p)
	if p and #p > 0 then
		self.textcolor.r = p[1] or 255
		self.textcolor.g = p[2] or 255
		self.textcolor.b = p[3] or 255
		if p[4] then self.textcolor.alpha = p[4] end	--// set alpha only in case it was explicitly stated
	else
		self.textcolor.r, self.textcolor.g, self.textcolor.b = 255, 255, 255
	end
end

function typewriter:setAlign(align)
	self.align = align or "left"
end