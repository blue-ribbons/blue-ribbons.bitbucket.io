local json = require("libs/dkjson")

local saveLoad = {}

saveload.saveTable = function(dtable, name)
	if dtable then
		love.filesystem.write(name, json.encode(dtable))
	end
end

saveload.loadTable = function(name)
	local data = love.filesystem.read(name)
	if data then
		local dtable = json.decode(data)
		return dtable
	end
	return nil
end