--// All possible game actions are registered here.

local signal = require("libs/hump/signal")

signal.register('move', function(dir, player)
	
	player.dir = dir
	player:move()
	
end)