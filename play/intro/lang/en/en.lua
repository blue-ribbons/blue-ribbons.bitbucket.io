local lang = {}

lang.sound = { [1] = "Music", [2] = "Sound Effects" }

lang.options = { [1] = "Language", [2] = "Music", [3] = "Sound Effects", [4] = "Typing Speed" }

lang.switch = { [1] = "Enabled", [2] = "Disabled" }

lang.menu = {
	[0] = {
		["true"] = {
			[1] = "LOAD GAME",
			[2] = "NEW GAME",
			[3] = "OPTIONS",
			[4] = "EXIT",
		},
		["false"] = {
			[1] = "NEW GAME",
			[2] = "OPTIONS",
			[3] = "EXIT",
		},
	},
	[1] = {
		["true"] = {
			[1] = "LOAD GAME",
			[2] = "NEW GAME",
			[3] = "EXTRA",
			[4] = "OPTIONS",
			[5] = "EXIT",
		},
		["false"] = {
			[1] = "NEW GAME",
			[2] = "EXTRA",
			[3] = "OPTIONS",
			[4] = "EXIT",
		},
	},
	
}

lang.gamemenu = {

	[1] = "SAVE",
	[2] = "LOAD",
	[3] = "MAIN MENU",

}

lang.extra = {
	[1] = "GALLERY",
	[2] = "MUSIC PLAYER",
}

lang.start = "Start"
lang.back = "Back"
lang.empty = "<empty>"
lang.locked = "<locked>"

lang.double_esc = "Press once again to exit"

return lang