local src = {}
src[1] = '"I wonder if Chiyoʼs going to be mad at me for being this late. Had a deadline...sheʼll understand. Right?"'
src[2] = '"In any case, Iʼll work on my next idea for my manga while I wait.\nI think Ken-san will really like my idea about a fantasy side-story involving Suzuki as a crime-solving detective. Maybe Iʼll have a tanuki cameo at the end of each murder.\nHmm..."'
return src