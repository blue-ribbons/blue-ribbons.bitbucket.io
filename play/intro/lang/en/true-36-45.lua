local src = {}
src[1] = '"A very good idea indeed, Chiyo-chan," Nozaki patts her head. "But I think it takes too much advantage of the readers and making everything that happens amount to a mere dream is too sad." '
src[2] = '"Going to go to the restroom real quick."\nNozaki stands up.'
src[3] = '"There canʼt possibly be an alternate me with a different color ribbon. If anything, itʼd just be me wearing a different pair.'
src[4] = 'Impossible, right?" - Chiyo chuckles innocently to herself.'
src[5] = '"I wonder what color ribbon Nozaki-kun would want me to wear."\nChiyo blushes, looking down at nothing in particular.'
src[6] = '"Whatʼs taking Nozaki-kun so long?"'
return src
