local src = {}
src[1] = '"Should I really end it like that?" - Nozaki mumbles to himself in his slumber.\n"Is it too happy of an ending? Hmm, it may seem a bit unrealistic after all."'
src[2] = '"But if I end everything that happened as a dream, theyʼll call me a talentless hack. Maybe I should call Chiyo-chan and see what she thinks."'
src[3] = '"And if I donʼt end it in the near future, theyʼre going to say Iʼm milking my work for all itʼs worth."'
return src
