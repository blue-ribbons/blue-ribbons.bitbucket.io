GS = require("libs/hump/gamestate")
br = require("src/blue_ribbons")
local state = require("states/gamescreen")
--require("libs/tlfres")
require ("libs/tesound/TEsound")
require("libs/gui/init")
--require("libs/lovedebug")

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local background = br.background

about = {
	ver = "1.0",
	index = "pv",
	type = 1,	--// extra stuff enabled
	scene = "main",
	settings = {
		sound = { sfx = true, bgm = true },
		lang = "en",
		gpswitch = true,
		textspd = 0.02,
	},
	session = {
		cslot = {
			shadow = {},	--// shadow image
		},			--// current slot	
	--	pgr_image = {},		--// pager image
	},
}

local sW, sH = love.graphics.getWidth(), love.graphics.getHeight()

gamestates = {}

function love.load()
--	TLfres.setScreen({ w = 1152, h = 864, full = false, aa = 0 }, 1152)
--	love.window.setIcon(love.image.newImageData("graphics/static/blue_ribbons_icon.png"))
	GS.registerEvents()	
--	GS.switch(require("states/intro"))
	about.session.cslot.script = "intro-1"
	about.session.cslot.shadow = {}
	GS.switch(state)
end

function love.draw()
	
end

function love.update(dt)
	TEsound.cleanup()
end
--[[
function love.mousepressed(x, y, button)  gui.pressed() end
function love.mousereleased(x, y, button) gui.released() end
function love.keypressed(key)      gui.keypressed(key) end

function love.touchpressed(id, x, y, pressure)  gui.pressed (id, x, y) end
function love.touchmoved(id, x, y, pressure)    gui.moved   (id, x, y) end
function love.touchreleased(id, x, y, pressure) gui.released(id, x, y) end		]]