local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local credit_roll = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc

function credit_roll:init()
	self.print = {}
	self.font = br.fonts["regular"]
	self.logo = love.graphics.newImage("graphics/static/logo_liminalia2.png")
end

function credit_roll:enter(state, mode)
	about.scene = "credit_roll"
	
	self.mode = mode
	self.alpha = 255
	self.ty1 = sH
	self.ty2 = sH
	self.ly = sH + 130 * sc
	self.state = 0
	self.text_buffer_1 = ""
	self.text_buffer_2 = ""
	self.timer = timer.new()
	
	if self.mode == 1 then
		t = 25
		local upperlim = -(sH * 2)
		local data
		if love.filesystem.exists("battery_save") then
			local raw = love.filesystem.read("battery_save")
			data = json.decode(raw)
			data.route2 = 1		--// enable route "Rebuild"
		else
			data = {
				route2 = 1, 
				slots = { 
					[1] = { scene = lang.empty },
					[2] = { scene = lang.empty },
					[3] = { scene = lang.empty },
					[4] = { scene = lang.empty },
					[5] = { scene = lang.empty },
				},
				achievements = { 
					img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
					bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
				}			
			}
		end
		love.filesystem.write("battery_save", json.encode(data))

		self.text_buffer_1 = "ORIGINAL CREATOR\n\
		/a/nonymous\n\
		\n\
		\n\
		SOURCE\n\
		https://archive.moe/a/thread/115493076/\n\
		\n\
		\n\
		CG DESIGN AND GRAPHICS\n\
		/a/nonymous\n\
		\n\
		\n\
		\n\
		\n\
		Route 'Rebuild' has been unlocked!\
		"
		self.text_buffer_2 = "See you in the Act II!"
		
		self.bgm = TEsound.play("sound/bgm/why_or_why_not.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		
		self.timer:tween(15, self, { ty1 = upperlim }, "linear", function()
			self.timer:tween(5, self, { ty2 = sH / 2 }, "linear", function()
				self.timer:tween(4, self, { alpha = 0 }, "linear")
			end)
		end)
		
		self.timer:addPeriodic(25, function()
			TEsound.stop("bgm")
			TEsound.play("sound/bgm/01_faint_voice.ogg", "bgm")
			GS.switch(require("states/mainmenu"))
		end)
		
	elseif self.mode == 2 then
		t = 55		
		local upperlim = -sH * 5 - 128 * sc
		
		self.text_buffer_1 = "ORIGINAL CREATOR\n\
		/a/nonymous\n\
		\n\
		\n\
		SOURCE\n\
		https://archive.moe/a/thread/115493076/\nhttps://archive.moe/a/thread/115534184/\nhttps://archive.moe/a/thread/115560076/\n\
		\n\
		\n\
		CG DESIGN AND GRAPHICS\n\
		/a/nonymous\n\
		\n\
		\n\
		BACKGROUND MUSIC\n\
		/a/nonymous (Liminalia) - Faint Voice\
		Tokisawa Nao - Strange Feeling (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - Fatal Scene (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - School Days (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - Nokosare ta Jikan (Gokukoku no Brynhildr OST)\
		Konishi Kayo & Kondoo Yukio – Katsubou (Elfen Lied OST)\
		Haga Keita - Ever-present Feeling (Fate/stay night OST)\
		Haga Keita - Motome Au Kokoro (Fate/stay night OST)\
		Kajiura Yuki - Silver Moon (Fate/Zero OST)\
		Iwasaki Motoyoshi - Angeloid no Senaka (Sora no Otoshimono OST)\
		Hashimoto Yukari – Lost My Pieces (Toradora OST)\
		ORANGE RANGE – Asterisk (Bleach OP1)\
		FLOW – Sign (Naruto Shippuuden OP6)\
		Linked Horizon – Guren no Yumia (Shingeki no Kyojin OP1)\
		Oshima Hiroyuki feat. Katakiri Rekka – Why, Or Why Not (Higurashi no Naku Koro ni ED)\
		ClariS - CLICK (Nisekoi OP) (piano cover by yoshiki - http://www.youtube.com/channel/UC_2qXn25z1EZM_0vB-90R_g)\n\
		\n\
		\n\
		SOUND EFFECTS\n\
		/a/nonymous (Liminalia)\nFate/stay night OST\n\
		\n\
		\n\
		PROGRAMMING\n\
		/a/nonymous (Liminalia)\n\
		\n\
		\n\
		LÖVE\nhttps://love2d.org/\n\
		\n\
		\n\
		LIBRARIES\n\
		David Kolf's JSON module for Lua 5.1/5.2\
		GOOi by Tavo\
		HUMP by vrld\
		TEsound by Ensayia & Taehl\
		LoveDebug 1.4.0 by kalle2990/Ranguna259\n\
		\n\
		\n\
		\n\
		\n\
		\n\
		"
--		\n\2012-2015\
--		http://liminalia-dev.blogspot.com/\n\	
	
		self.text_buffer_2 = "The End\n[spoiler]:^)[/spoiler]"
		
		self.bgm = TEsound.play("sound/bgm/why_or_why_not2.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		self.timer:tween(42, self, { ty1 = upperlim }, "linear", function()
			self.timer:tween(7, self, { ty2 = sH / 2 }, "linear", function()
				self.timer:tween(5, self, { alpha = 0 }, "linear")
			end)
		end)
		
		self.timer:addPeriodic(32.5, function()
			self.timer:tween(42, self, { ly = upperlim }, "linear")
		end)
		
		self.timer:addPeriodic(55, function()
			TEsound.stop("bgm")
			TEsound.play("sound/bgm/01_faint_voice.ogg", "bgm")
			GS.switch(require("states/mainmenu"))
		end)
		
	end
	
end

function credit_roll:leave()

	self.timer:clear()
end

function credit_roll:update(dt)

	self.timer:update(dt)
end

function credit_roll:draw()
	love.graphics.draw(self.logo, sW / 2, self.ly, 0, sc, sc, self.logo:getWidth() / 2, self.logo:getHeight() / 2)
	love.graphics.setColor(255, 255, 255, self.alpha)
	love.graphics.printf(self.text_buffer_1, 0, self.ty1, sW, 'center')
	love.graphics.printf(self.text_buffer_2, 0, self.ty2, sW, 'center')
end

return credit_roll