local timer = require("libs/hump/timer")
local extra = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function extra:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.font = br.fonts["large"]
	self.sfx = br.sfx
	self.timer = timer.new()
	
end

function extra:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "extra"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.menu = { labels = lang.extra, buttons = {}, active = 1 }
	self.menu.actions = {
		[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/gallery"))	end,
		[2] = function()	TEsound.play(self.sfx[1], "click", 0.6)	TEsound.stop("bgm")	GS.switch(require("states/mplayer"))	end,
		[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/mainmenu"))	end,
	}
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do		
		gui.newButton("extra-"..i, self.menu.labels[i], sW / 2 - 140 * sc, (340 + delta * i) * sc, 280 * sc, 60 * sc)
		table.insert(self.menu.buttons, "extra-"..i)	
	end
	for i = 1, #self.menu.buttons, 1 do	
		gui.get(self.menu.buttons[i]).fgColor, gui.get(self.menu.buttons[i]).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	gui.newButton("extra-3", lang.back, sW / 2 - 130 * sc, 680 * sc, 260 * sc, 60 * sc)
	gui.get("extra-3").fgColor, gui.get("extra-3").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("extra-3"):setRadiusCorner(gui.get("extra-3").h / 2)
	gui.get("extra-3"):onRelease(self.menu.actions[3])
	table.insert(self.menu.buttons, "extra-3")
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
--	self.input = "none"
	self.joystickBlocked = false
	
end

function extra:leave()
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("extra-"..i)
	end
end

function extra:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function extra:draw()

	love.graphics.setFont(self.font)
	love.graphics.printf("Blue Ribbon Monogatari", 0, 120, sW, 'center')
	gui.draw()
	
	drawFPS()
	
end

function extra:keypressed(key, unicode)
	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[3]()	end,
		["menu"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[3]()	end,
	}
	if switch[key] then switch[key]()	end
--	self.input = key
end

function extra:joystickpressed(joystick, button)

	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[3]()		end,
	}
	if switch[button] then switch[button]()	end
	
end

return extra