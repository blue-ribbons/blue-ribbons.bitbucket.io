local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local mplayer = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function mplayer:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.sfx = br.sfx
	self.timer = timer.new()
	
end

function mplayer:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "mplayer"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	self.current_bgm = 0
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = lang.empty },
			[2] = { scene = lang.empty },
			[3] = { scene = lang.empty },
			[4] = { scene = lang.empty },
			[5] = { scene = lang.empty },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
		},
	}				
	if love.filesystem.exists("battery_save") then
		local data = love.filesystem.read("battery_save")
		savefile = json.decode(data)
	end
	
	local labels = {
		[1] = "Strange Feeling",
		[2] = "Fatal Scene", 
		[3] = "Katsubou",	
		[4] = "School Days",
		[5] = "Nokosare ta Jikan", 
		[6] = "Ever-present Feeling",
		[7] = "Angeloid no Senaka",
		[8] = "Silver Moon",	
		[9] = "Motome Au Kokoro",
		[10] = "Click (Piano Cover)",
	}
	
	self.menu = { buttons = {}, active = 1 }
	
	self.titles = {
		[0] = "Nobody - None",
		[1] = "Tokisawa Nao - Strange Feeling (Gokukoku no Brynhildr OST)",
		[2] = "Tokisawa Nao - Fatal Scene (Gokukoku no Brynhildr OST)",
		[3] = "Konishi Kayo & Kondoo Yukio – Katsubou (Elfen Lied OST)",
		[4] = "Tokisawa Nao - School Days (Gokukoku no Brynhildr OST)",
		[5] = "Tokisawa Nao - Nokosare ta Jikan (Gokukoku no Brynhildr OST)",	
		[6] = "Haga Keita - Ever-present Feeling (Fate/stay night OST)",
		[7] = "Iwasaki Motoyoshi - Angeloid no Senaka (Sora no Otoshimono OST)",
		[8] = "Kajiura Yuki - Silver Moon (Fate/Zero OST)",
		[9] = "Haga Keita - Motome Au Kokoro (Fate/stay night OST)",
		[10] = "ClariS - CLICK (Nisekoi OP) (piano cover by yoshiki)",
	}
	
	self.src = {
		[1] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/strange_feeling.ogg",
		},
		[2] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/fatal_scene.ogg"
		},
		[3] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/katsubou.ogg"
		},
		[4] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/school_days.ogg"
		},
		[5] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/nokosare_ta_jikan.ogg"
		},
		[6] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/bgm16.ogg"
		},
		[7] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/angeloid_no_senaka.ogg"
		},
		[8] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/silver_moon.ogg"
		},
		[9] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/bgm19.ogg"
		},
		[10] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/click_cover_cut.ogg"
		},
	}
	
	for i = 1, #savefile.achievements.bgm, 1 do
		if savefile.achievements.bgm[i] == 1 then
			self.src[i][1] = 1
			self.src[i][2] = labels[i]
		end
	end
	
	self.action = function(id)
		if id ~= 11 then
			if self.src[id][1] == 1 then
				TEsound.play(self.sfx[1], "click", 0.6)
				TEsound.stop("bgm")
				if self.current_bgm == id then
					self.current_bgm = 0
				else
					self.current_bgm = id
					TEsound.play(self.src[id][3], "bgm", _, _, function() self.current_bgm = 0 end)
					TEsound.volume("bgm", 0.5)
				end
			else
				TEsound.play(self.sfx[1], "click", 0.6)
			end
		else
			self.quit()
		end
	end
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		TEsound.stop("bgm")
		TEsound.playLooping("sound/bgm/01_faint_voice.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		GS.switch(require("states/mainmenu"))
	end
	
	local delta = 70
	for i = 1, 5, 1 do		
		gui.newButton("mplayer-"..i, self.src[i][2], sW / 2  - 360 * sc + 32 * sc, (180 + delta * i) * sc, 312 * sc, 60 * sc)
		table.insert(self.menu.buttons, "mplayer-"..i)	
	end
	
	delta = 70
	for i = 6, 10, 1 do		
		gui.newButton("mplayer-"..i, self.src[i][2], sW / 2  + 360 * sc - 312 * sc - 32 * sc, (180 + delta * (i - 5)) * sc, 312 * sc, 60 * sc)
		table.insert(self.menu.buttons, "mplayer-"..i)	
	end
	
	
	for i = 1, #self.menu.buttons, 1 do	
		gui.get(self.menu.buttons[i]).fgColor, gui.get(self.menu.buttons[i]).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get(self.menu.buttons[i]):onRelease(function() self.action(i) end)
	end
	
	gui.newButton("mplayer-11", lang.back, sW / 2 - 130 * sc, 680 * sc, 260 * sc, 60 * sc)
	gui.get("mplayer-11").fgColor, gui.get("mplayer-11").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("mplayer-11"):setRadiusCorner(gui.get("mplayer-11").h / 2)
	gui.get("mplayer-11"):onRelease(self.quit)
	table.insert(self.menu.buttons, "mplayer-11")
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
--	self.input = "none"
	self.joystickBlocked = false
	
end

function mplayer:leave()
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("mplayer-"..i)
	end
end

function mplayer:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function mplayer:draw()
	love.graphics.printf(self.titles[self.current_bgm], 0, 128 * sc, sW, "center")
	gui.draw()
	drawFPS()	
end

function mplayer:keypressed(key, unicode)
	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.quit()	end,
		["menu"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
--	self.input = key
end

function mplayer:joystickpressed(joystick, button)

	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end
	
end

return mplayer