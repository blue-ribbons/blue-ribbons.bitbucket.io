local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local os_date = os.date
local menuload = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function menuload:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.font = br.fonts["large"]
	self.timer = timer.new()
	self.sfx = br.sfx
end

function menuload:enter(state)
	about.scene = "menuload"
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.to_menu = function()
	--	about.session.pgr_image:rewind()
--		TEsound.stop("bgm")
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/mainmenu"))
	end
	
	gui.newButton("menu_btn", _, xc - 100 * sc, 18 * sc, 82 * sc, 82 * sc, "graphics/static/gear.png", "save", sc)
	gui.get("menu_btn").fgColor, gui.get("menu_btn").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("menu_btn"):onRelease(function() self.to_menu() end)
	
	self.menu = { buttons = {}, active = 1 }
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = lang.empty },
			[2] = { scene = lang.empty },
			[3] = { scene = lang.empty },
			[4] = { scene = lang.empty },
			[5] = { scene = lang.empty },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, }, 
		},
	}				
	if love.filesystem.exists("battery_save") then
		local data = love.filesystem.read("battery_save")
		savefile = json.decode(data)
	end
	
	self.action = function(id)
--[[	TEsound.play(self.sfx[1], "click", 0.6)
		about.session.cslot.timestamp = os_date("%c")
		savefile.slots[id] = about.session.cslot
		love.filesystem.write("battery_save", json.encode(savefile))
		self.quit()
		]]
		
		if savefile.slots[id]["scene"] ~= "<empty>" and savefile.slots[id]["scene"] ~= "<пусто>" then --lang.empty then
	--		about.session.pgr_image = nil
			about.session.cslot = savefile.slots[id]
			
			TEsound.stop("bgm")
			TEsound.play(self.sfx[1], "click", 0.6)	
			GS.switch(require("states/gamescreen"), true)
		end
		
	end
	
	local meta, delta = {}, 0
	for i = 1, #savefile.slots, 1 do
		meta[i] = {}
		meta[i]["label"] = savefile.slots[i]["scene"]
		if savefile.slots[i]["scene"] == "<empty>" or savefile.slots[i]["scene"] == "<пусто>" then --lang.empty then 
			meta[i]["img"] = "empty_slot"
		else
			meta[i]["label"] = meta[i]["label"].." - "..savefile.slots[i]["timestamp"]
			meta[i]["img"] = "icon_r"..savefile.slots[i]["route"]
		end
		meta[i]["delta"] = 140 * i
	end
	
	for i = 1, #meta, 1 do
		gui.newButton("save-"..i, meta[i]["label"], sW / 2 - 350 * sc, (meta[i]["delta"] - 45) * sc, 700 * sc, 112 * sc, "graphics/static/"..meta[i]["img"]..".png", "save", sc)
		gui.get("save-"..i).fgColor, gui.get("save-"..i).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get("save-"..i):onRelease(function()	self.action(i)	end)
		table.insert(self.menu.buttons, "save-"..i)
	end
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
	
end

function menuload:leave()
	self.timer:clear()
end

function menuload:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end			
	end
	self.timer:update(dt)
	
end

function menuload:draw()
	
--	TLfres.transform()
	
	gui.draw("save")
	
	drawFPS()
	
end

function menuload:leave()
	
	self.timer:clear()
	
	gui.remove("menu_btn")
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("save-"..i)
	end
	
end

function menuload:keypressed(key, unicode)

	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.to_menu()	end,
	}
	if switch[key] then switch[key]()	end
	
end

function menuload:joystickpressed(joystick, button)
	
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
		
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.to_menu()			end,
	}
	if switch[button] then switch[button]()	end
	
end

return menuload