local timer = require("libs/hump/timer")
local mainmenu = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function mainmenu:init()
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	self.font = br.fonts["large"]
	self.timer = timer.new()
	self.sfx = br.sfx
	self.bgm = TEsound.playLooping("sound/bgm/01_faint_voice.ogg", "bgm")
	TEsound.volume(self.bgm, 0.5)
	self.esc_color = { 0, 191, 255, 0 }
	self.esc_enabled = false
	
end

function mainmenu:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "mainmenu"
	
	self.esc_text = lang.double_esc
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.double_esc = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		if self.esc_enabled then
			GS.switch(require("states/exit"))
		else
			self.esc_enabled = true
			self.esc_color[4] = 255
			self.timer:tween(4,  self.esc_color, {	[4] = 0	}, "linear", function() self.esc_enabled = false	end)
		end
	end
	
	local actions = {
		[0] = {
			["true"] = {	-- there is a saved progress
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/menuload"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[4] = function()	GS.switch(require("states/exit"))	end,
			},
			["false"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[3] = function()	GS.switch(require("states/exit"))	end,
			}
		},
		[1] = {
			["true"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/menuload"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/extra"))	end,
				[4] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[5] = function()	GS.switch(require("states/exit"))	end,
			},
			["false"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/extra"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[4] = function()	GS.switch(require("states/exit"))	end,
			}
		}
	}
	
	self.menu = { labels = {}, buttons = {}, active = 1 }
	self.menu.labels = lang.menu[about.type][tostring(love.filesystem.exists("battery_save"))]
	self.menu.actions = actions[about.type][tostring(love.filesystem.exists("battery_save"))]
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do
		
		gui.newButton("menu-"..i, self.menu.labels[i], sW / 2 - 120 * sc, (340 + delta * i) * sc, 240 * sc, 60 * sc)
		table.insert(self.menu.buttons, "menu-"..i)
		
	end
	
	for i = 1, #self.menu.buttons, 1 do
		
		gui.get(self.menu.buttons[i]).fgColor, gui.get(self.menu.buttons[i]).bgColor = self.uiColor.fg, self.uiColor.bg
		gui.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	
	
	self.onKeyDown = function()	
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gui.get(self.menu.buttons[self.menu.active]).bgColor = self.uiColor.bg
			self.menu.active = #self.menu.buttons
		end
	end
	
--	self.input = "none"
	self.joystickBlocked = false
	
end

function mainmenu:leave()
	
--	self.timer:clear()
	for i = 1, #self.menu.buttons, 1 do
		gui.remove("menu-"..i)
	end
	
end

function mainmenu:update(dt)
	
	gui.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function mainmenu:draw()
	
--	TLfres.transform()
	
--	love.graphics.print("last input event: "..self.input)

	love.graphics.setFont(self.font)
	love.graphics.printf("Blue Ribbon Monogatari", 0, 120, sW, 'center')
	gui.draw()
	if about.settings.gpswitch then
		gui.draw("gamepad")
	end
	
	if self.esc_enabled then
		love.graphics.setColor(self.esc_color)
		love.graphics.printf(self.esc_text, sW / 2 - 200 * sc, sH - 100 * sc, 400 * sc, 'center')
	end
	
	love.graphics.setColor(255, 255, 255)
	
	drawFPS()
	
end

function mainmenu:keypressed(key, unicode)
	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.double_esc()	end,
		["menu"] = function()	self.double_esc()	end,
	}
	if switch[key] then switch[key]()	end
--	self.input = key
end

function mainmenu:joystickpressed(joystick, button)

--[[	
	if joystick:isGamepadDown('dpup') then
		TEsound.play(self.sfx, "switch")
		self.onKeyUp()
	elseif joystick:isGamepadDown('dpdown')	then
		TEsound.play(self.sfx, "switch")
		self.onKeyDown()
	end	]]
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.double_esc()		end,
	}
	if switch[button] then switch[button]()	end
--	self.input = button
	
end

return mainmenu