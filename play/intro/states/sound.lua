local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local sound = {}
local lang

local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS

function sound:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "sound"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.sfx = br.sfx
	
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
	
	self.menu = { sections = lang.sound, labels = lang.switch, radio = { [1] = true, [2] = false }, buttons = {}, actions = {}, active = 1 }
	self.radioGroup = {}
	
	local delta = 70
	for i = 1, #self.menu.labels, 1 do	
		gui.newRadio("sound-"..i, self.menu.labels[i], sW / 2 - 130 * sc, (170 + delta * i) * sc, 260 * sc, 60 * sc, self.menu.radio[i], "bgm", "sound-radio")
		gui.get("sound-"..i).fgColor, gui.get("sound-"..i).bgColor = self.uiColor.fg, self.uiColor.bg
		table.insert(self.menu.buttons, "sound-"..i)
	end
	
	for i = 1, #self.menu.labels, 1 do	
		gui.newRadio("sound-"..(i + 2) , self.menu.labels[i], sW / 2 - 130 * sc, (380 + delta * i) * sc, 260 * sc, 60 * sc, self.menu.radio[i], "sfx", "sound-radio")
		gui.get("sound-"..(i + 2)).fgColor, gui.get("sound-"..(i + 2)).bgColor = self.uiColor.fg, self.uiColor.bg
		table.insert(self.menu.buttons, "sound-"..(i + 2))
	end
	
	gui.newButton("sound-5", lang.start, sW / 2 - 130 * sc, 640 * sc, 260 * sc, 60 * sc)
	gui.get("sound-5").fgColor, gui.get("sound-5").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("sound-5"):setRadiusCorner(gui.get("sound-5").h / 2)
	table.insert(self.menu.buttons, "sound-start")
	
	self.menu.actions[1] = function()
		local config = { settings = about.settings }
		love.filesystem.write("settings.cfg", json.encode(config))
		GS.switch(require("states/mainmenu"))
	end
	
	self.onKeyDown = function()	
		gui.get("sound-"..self.menu.active).bgColor = self.uiColor.bg		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gui.get("sound-"..self.menu.active).bgColor = self.uiColor.bg		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		
		if self.menu.active < #self.menu.buttons then
			gui.get("sound-"..self.menu.active):select()
		else
			self.menu.actions[1]()
		end
		
	end
	
	gui.get("sound-5"):onRelease(function(c) TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]() end)
	
	self.timer = timer.new()
	self.joystickBlocked = false
	
end

function sound:leave()
	self.timer:clear()
	gui.remove("sound-5")
end

function sound:update(dt)

	gui.get("sound-"..self.menu.active).bgColor = self.activeColor
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	about.settings.sound.bgm = gui.get("sound-1").selected
	about.settings.sound.sfx = gui.get("sound-3").selected
	
	self.timer:update(dt)
	
end

function sound:draw()
	
--	TLfres.transform()
	
	love.graphics.printf(lang.sound[1], 0, 190 * sc, sW, 'center')
	love.graphics.printf(lang.sound[2], 0, 400 * sc, sW, 'center')
	gui.draw()
	if about.settings.gpswitch then
		gui.draw("gamepad")
	end
	gui.draw("sound-radio")
	
	drawFPS()
	
end

function sound:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.onKeyUse()	end,
		[" "] = function()	self.onKeyUse()	end,
		["e"] = function()	self.onKeyUse()	end,
		["x"] = function()	self.onKeyUse()	end,
	}
	if switch[key] then switch[key]()	end
end

function sound:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.onKeyUse()	end,
		[2] = function()	self.onKeyUse()	end,
		[3] = function()	self.onKeyUse()	end,
		[4] = function()	self.onKeyUse()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end,
	--	[11] = function()	self.onKeyUse()	end
	}
	if switch[button] then switch[button]()	end
end

return sound