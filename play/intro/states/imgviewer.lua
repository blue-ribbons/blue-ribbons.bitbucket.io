local timer = require("libs/hump/timer")
local sW, sH, sc = br.sW, br.sH, br.sc
local cx, cy, xc, yc = br.cx, br.cy, br.xc, br.yc
local drawFPS = br.drawFPS
local imgviewer = {}

function imgviewer:init()
	self.sfx = br.sfx	
	self.timer = timer.new()
	self.uiColor = br.uiColor
	self.activeColor = br.activeColor
end

function imgviewer:enter(state, img)

	about.scene = "imgviewer"
	
	local joysticks = love.joystick.getJoysticks()
    self.joystick = joysticks[1]
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/gallery"))
	end
	
	gui.newButton("menu_btn", _, xc - 100 * sc, 18 * sc, 82 * sc, 82 * sc, "graphics/static/gear.png", "menu", sc)
	gui.get("menu_btn").fgColor, gui.get("menu_btn").bgColor = self.uiColor.fg, self.uiColor.bg
	gui.get("menu_btn"):onRelease(function() self.quit() end)
	
	self.image = love.graphics.newImage(img)
	self.width, self.height = self.image:getWidth(), self.image:getHeight()
	
end

function imgviewer:leave()
	gui.remove("menu_btn")
end

function imgviewer:update(dt)
	
	gui.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)
	
end

function imgviewer:draw()
	love.graphics.draw(self.image, self.width / 2, self.height / 2, 0, sc, sc, self.width / 2, self.height / 2)
	gui.draw("menu")	
	drawFPS()
end

function imgviewer:keypressed(key, unicode)	
	self.quit()
end

function imgviewer:joystickpressed(joystick, button)
	self.quit()
end

return imgviewer