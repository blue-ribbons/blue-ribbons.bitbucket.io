local src = require("lang/"..about.settings.lang.."/prank")
script = {
	[1] = { "set_page", { name = "Intro", image = "1" } },	
	[2] = { "print", { msg = src[3], mode = 0 } },
	[3] = { "input", { type = 0 }},
	[4] = { "fade_text", { dir = "in", time = 0.5 } },
	[5] = { "set_page", { name = "Prank", image = "2" }, { mode = "fade", time = 0.5 } },
	[6] = { "play", { mode = 1, name = "se414", loop = false } },
	[7] = { "set_page", { name = "Prank", image = "3" }, { mode = "fade", time = 0.5 } },
	[8] = { "set_page", { name = "Prank", image = "4" }, { mode = "fade", time = 0.5 } },
	[9] = { "set_page", { name = "Prank", image = "5" }, { mode = "fade", time = 0.5 } },
	[10] = { "set_page", { name = "Prank", image = "6" }, { mode = "fade", time = 0.5 } },
	[11] = { "fill", { mode = "in", time = 3, r = 0, g = 0, b = 255 } },
	[12] = { "set_page", { name = "Intro", image = "static/blank" } },
	[13] = { "fill", { mode = "out", time = 0.5, r = 0, g = 0, b = 255 } },
	[14] = { "play", { mode = 1, name = "rip", loop = false } },
	[15] = { "set_page", { name = "Prank", image = "7" }, { mode = "fade", time = 0.5 } },
	[16] = { "wait", { time = 3 } },
	[17] = { "fill", { mode = "in", time = 5 } },
--	[18] = { "redirect", 'http://boards.4chan.org/a/thread/131508246/' },
	[18] = { "redirect", 'http://desustorage.org/a/thread/131508246/#131518948' },
	
--[[	[4] = { "play", { mode = 1, name = "se221", loop = true } },
	[5] = { "fill", { mode = "out", time = 3 } },
	[6] = { "fade_text", { dir = "out", time = 1 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "input", { type = 1 }, { [1] = { text = src[2], name = "prank-2" }}},	]]
--	[63] = { "redirect", 'main.html' },	131518948
}
return script