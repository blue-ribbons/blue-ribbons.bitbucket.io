var selected_section_archive;
var selected_section_media;
var show = false;
var image_index = 0;

var _special = {
  xmas: "Christmas",
  valentine: "Valentine's",
  halloween: "Halloween"
}

function fetchItems(array, tag) {
  var t = [];
  for (var i = 0; i < array.length; i++) {
    if (array[i].tag == tag || tag == "all") {
      t.push(array[i]);
    }
  }
  return t;
}

function fetchArchiveSection(tag) {
  var t = fetchItems(_threads, tag);
  var archive = document.getElementById("archive");
  var a;
  var item_cont;
  var div;
  var label_cont;
  var label;
  var img;
  for (var i = 0; i < t.length; i++) {
    a = document.createElement("a");
    a.setAttribute("href", t[i].url);
    a.setAttribute("target", "_blank");
    // item container
    item_cont = document.createElement("div");
    item_cont.classList.add("item-container");
    // background image div
    div = document.createElement("div");
    div.setAttribute("style", "background-image: url(" + t[i].icon + ")");
    if (t[i].status == 0) {
      div.classList.add("greyed-out");
    }
    item_cont.appendChild(div);
    // overlay div
    div = document.createElement("div");
    div.classList.add("overlay");
    // div containing title, special label and date
    label_cont = document.createElement("div");
    label_cont.classList.add("label");
    // title div
    label = document.createElement("div");
    text = document.createTextNode(t[i].title);
    label.appendChild(text);
    label_cont.appendChild(label);
    // special label div
    if (t[i].special) {
      label = document.createElement("div");
      label.classList.add("special-" + t[i].special);
      text = document.createTextNode(_special[t[i].special] + " Special");
      label.appendChild(text);
      label_cont.appendChild(label);
    }
    // date label div
    label = document.createElement("div");
    label.classList.add("date")
    text = document.createTextNode(t[i].date);
    label.appendChild(text);
    label_cont.appendChild(label);
    // append containers
    div.appendChild(label_cont);
    item_cont.appendChild(div);
    if (t[i].status == 0) {
      img = document.createElement("img");
      img.src = "img/missing.png";
      item_cont.appendChild(img);
    }
    a.appendChild(item_cont);
    archive.appendChild(a);
  }
}

function fetchMediaSection(tag) {
  var t = fetchItems(_media, tag);
  var media = document.getElementById("media");
  var a;
  var item_cont;
  var div;
  var img;
  var text;
  for (var i = 0; i < t.length; i++) {
    a = document.createElement("a");
    a.setAttribute("href", t[i].url);
    a.setAttribute("target", "_blank");
    // item container
    item_cont = document.createElement("div");
    item_cont.classList.add("item-container");
    // background image div
    div = document.createElement("div");
    div.setAttribute("style", "background-image: url(" + t[i].icon + ")");
    item_cont.appendChild(div);
    // overlay div
    div = document.createElement("div");
    div.classList.add("overlay");
    label_cont = document.createElement("div");
    label_cont.classList.add("label");
    text = document.createTextNode(t[i].title);
    label_cont.appendChild(text);
    div.appendChild(label_cont);
    item_cont.appendChild(div);
    // content type icon
    img = document.createElement("img");
    img.src = "img/" + t[i].tag + ".png";
    item_cont.appendChild(img); 
    a.appendChild(item_cont);    
    media.appendChild(a);
  }
}

function clearSection(container) {
  while (container.firstChild) {
    container.removeChild(container.lastChild);
  }
}

function selectArchiveSection() {
  if (this != selected_section_archive) {
    selected_section_archive.classList.remove("selected");
    selected_section_archive = this;
    selected_section_archive.classList.add("selected");
    clearSection(document.getElementById("archive"));
    fetchArchiveSection(selected_section_archive.getAttribute("tag"));
  }
}

function selectMediaSection() {
  if (this != selected_section_media) {
    selected_section_media.classList.remove("selected");
    selected_section_media = this;
    selected_section_media.classList.add("selected");
    clearSection(document.getElementById("media"));
    fetchMediaSection(selected_section_media.getAttribute("tag"));
  }
}

function toggleScreenshots() {
  if (!show) {
    show = true;
    document.getElementById("screenshots").style.display = "flex";
    document.getElementById('show-hide').firstChild.nodeValue = "Hide";
  } else {
    show = false;
    document.getElementById("screenshots").style.display = "none";
    document.getElementById('show-hide').firstChild.nodeValue = "Screenshots";
  }
}

function imagePreviewClose() {
  document.getElementById("preview").style.display = "none";
}

function attachEvents() {
  var elements = document.getElementsByClassName("section-archive");
  for (var i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click", selectArchiveSection, false);
  }
  elements = document.getElementsByClassName("section-media");
  for (i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click", selectMediaSection, false);
  }
  document.getElementById("show-hide").addEventListener("click", toggleScreenshots, false);
  document.getElementById("preview-close").addEventListener("click", imagePreviewClose, false);
}

function initArchiveSections() {
  var elements = document.getElementsByClassName("section-archive");
  for (var i = 0; i < elements.length; i++) {
    if (elements[i].getAttribute("tag") == "s1") {
      elements[i].classList.add("selected");
      selected_section_archive = elements[i];
      fetchArchiveSection("s1");
    }
  }
}

function initMediaSections() {
  var elements = document.getElementsByClassName("section-media");
  for (var i = 0; i < elements.length; i++) {
    if (elements[i].getAttribute("tag") == "all") {
      elements[i].classList.add("selected");
      selected_section_media = elements[i];
      fetchMediaSection("all");
    }
  }
}

function imagePreview(id) {
  console.log(id);
  console.log(image_index);
  image_index = id;
  document.getElementById("preview-img").src = _screens[id];
  document.getElementById("preview").style.display = "block";
}

function nextImage(n) {
  image_index = image_index + n;
  if (image_index > _screens.length - 1) {
    image_index = 0;
  } else if (image_index < 0) {
    image_index = _screens.length - 1;
  }
  imagePreview(image_index);
}

function initScreenshots() {
  var screenshots = document.getElementById("screenshots");
  var a;
  var div;
  for (var i = 0; i < _screens.length; i++) {
    a = document.createElement("a");
    a.setAttribute("onclick", "imagePreview(" + i + ")");
    // item container
    item_cont = document.createElement("div");
    item_cont.classList.add("item-container");
    // background image div
    div = document.createElement("div");
    div.setAttribute("style", "background-image: url(" + _screens[i] + ")");
    item_cont.appendChild(div);
    // overlay div
    div = document.createElement("div");
    div.classList.add("overlay");
    item_cont.appendChild(div);
    a.appendChild(item_cont);
    screenshots.appendChild(a);
  }
}

window.addEventListener("load", attachEvents);
window.addEventListener("load", initArchiveSections);
window.addEventListener("load", initMediaSections);
window.addEventListener("load", initScreenshots);