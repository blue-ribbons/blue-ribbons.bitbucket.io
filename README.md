Archive of Blue Ribbons threads and related content hosted at:
  
~~blueribbons.moe~~ >  
~~blueribbons.aerobatic.io~~ >  
~~blueribbonsarchive.bitbucket.io~~ >  
[blue-ribbons.bitbucket.io](https://blue-ribbons.bitbucket.io) (current)  
  
Old commits reside at [https://bitbucket.org/blue-ribbons/blue-ribbons.bitbucket.io-old](https://bitbucket.org/blue-ribbons/blue-ribbons.bitbucket.io-old).